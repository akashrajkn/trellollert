# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-22 05:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trellodemo', '0005_remove_card_last_modified'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=1000)),
                ('created_by', models.CharField(max_length=255)),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trellodemo.Card')),
            ],
        ),
        migrations.RemoveField(
            model_name='list',
            name='card',
        ),
        migrations.DeleteModel(
            name='List',
        ),
    ]
